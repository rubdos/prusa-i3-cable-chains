# explicit wildcard expansion suppresses errors when no files are found
include $(wildcard *.deps)

all: bed_mount.stl y_mount.stl z_motor_mount.stl chain_unit.stl  z_fixed_mount.stl

%.stl: %.scad
	openscad -m make -o $@ -d $@.deps $<
