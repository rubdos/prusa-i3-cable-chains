[![Flattr this Thing](https://button.flattr.com/flattr-badge-large.png)](https://flattr.com/submit/auto?fid=x21zd7&url=https%3A%2F%2Fgitlab.com%2Frubdos%2Fprusa-i3-cable-chains)

 [![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

# Cable chains for Prusa i3

Based on [Cable chain for Replicator Dual Xmotor by stephenmhall](http://www.thingiverse.com/thing:124700), published Jul 28, 2013, which is licensed under the Creative Commons - Attribution license.

I'm remixing [this cable chain](http://www.thingiverse.com/thing:124700) so it would fit on several places on my Prusa i3.

I had to remake the chain unit, because I couldn't repair the part so it would work in OpenSCAD. I remade it in OpenSCAD, sources are attached. I recommend you NOT to use the chain unit I made, but to use the original (the file `cable_chainunit.stl`, included).

**Z-axis**: Currently, there's only the z-axis mount (`z_fixed_mount.stl` or `z_fixed_mount.scad`) that goes on the back of the left z-axis motor (it reuses the screws you use to mount your Z-axis motor to the wooden/alu plate). Make sure to print it with enough cooling, especially when it's building the overhang for the second M3 hole.

I'm currently drawing the Z-axis motor mount, as having a cable chain with only one half of the chain attached is kinda useless. I'm also printing more `chain_unit`s. Will upload a non-potato photo when it's finished.

**Y-axis**: TODO

**X-axis**: TODO

Development is on [my GitLab profile](https://gitlab.com/rubdos/prusa-i3-cable-chains), feel free to fork and make merge requests. Builds are available for download on [Thingiverse](http://www.thingiverse.com/thing:1290122). I'll regularly sync this README file there.

