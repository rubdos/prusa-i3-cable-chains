$fn=100;
width=15.5;
wall=2;
margin=1;
height=16;
length=18;
base_length=11;
inner_height=7;
extension_length=19;
overlap=6;

module base_cutouts() {
    translate([base_length, -width/2, 0]) {
        cube([length-base_length,width, height]);
        rotate([0, 45, 0]) {
            translate([0, -wall, 0])
            cube([100, width + 2*wall, 100]);
        }
    }
    rotate([90, 0, 90]) {
        linear_extrude(height = base_length+margin, convexity = 10, slices = 20, scale = 1.0)
        polygon(points=[[0,margin], [-width/2, inner_height], [-width/2, height-wall], [width/2, height-wall], [width/2, inner_height]]);
    }
}

module cone() {
    rotate([0, -90, 0])
        cylinder(r2=0, r1=5/2, h=wall, center=true);
}

module floor_extension() {
    translate([-(length-base_length-margin), -(width/2-margin), height-wall])
    cube([length-base_length-margin, width-2*margin, wall]);
}

module sides() {
    straight_len=extension_length-height/2;
    for(side=[1, -1]) {
        difference() {
            union(){
                translate([-straight_len/2+overlap, side*(width/2 + wall*1.5 +0.5), height/2]) {
                    cube([straight_len, wall, height], center=true);
                    translate([-height/4-straight_len/2, 0, height/4]) {
                        cube([height/2, wall, height/2], center=true);
                    }
                    translate([-straight_len/2, 0, 0]) {
                        rotate([90, 0, 0])
                        cylinder(r=height/2, h=wall, center=true);
                    }
                }
            }
            translate([-straight_len/2+overlap, side*(width/2 + wall*1.5 + 0.5), height/2]) {
                translate([-straight_len/2 -0.5, -margin/2*side, 0]) {
                    rotate([0, 0, -side*90]){
                        cone();
                    }
                }
            }
        }
        translate([0, (width/2 + wall*1.5 - 1), 0]) {
            cube([overlap, 0.5, height]);
        }
        translate([0, -(width/2 + wall*1.5 - 1 + .5), 0]) {
            cube([overlap, 0.5, height]);
        }
    }
}
module chain_unit() {
    union() {
        difference() {
            union() {
                translate([0, -(width + 2*wall)/2, 0])
                    cube([length, width + 2*wall, height]);
                for(side=[1, -1]) {
                    translate([base_length, side*(width/2 + wall*1.5), height/2]) {
                        translate([4.5, -margin/2*side, 0]) {
                            rotate([0, 0, -side*90]){
                                cone();
                            }
                        }
                    }
                }
            }
            base_cutouts();
        }
        floor_extension();
        sides();
}
}

rotate([0,180,0])
    chain_unit();