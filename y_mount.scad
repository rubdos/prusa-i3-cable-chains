use <chain_unit.scad>;

plate_width = 20 + 9.75 + 5;
plate_height = 10;

module m3() {
    $fn=100;
    rotate([-90, 0, 0])
    cylinder(r=1.5, h=50);
}

rotate([0, 90, 0])
union() {
    difference() {
        translate([0, (20 + 4 + 0.5)/2, 6]) {
            rotate([0, 90, 0])
                chain_unit();
        }
        translate([0, 0, -40])
            cube([40, 40, 40]);
    }
    difference() {
        translate([-plate_width + 16, 0, -plate_height])
            cube([plate_width, 2, plate_height]);
        translate([16, 0, -plate_height/2]) {
            translate([-9.5, 0, 0])
                m3();
            translate([-9.5-20, 0, 0])
                m3();
        }
    }
}