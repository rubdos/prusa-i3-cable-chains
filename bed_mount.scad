use <chain_unit.scad>;

distance=80;
top_margin = 18;
margin = 1;
inf = 150;
length = 40 + 18;

width = distance/2 + 20 + 4 + 0.5;

module connectors() {
    dx = distance/2;

    translate([0, dx/2, 0])
    chain_unit();

    mirror([0, 1 , 0])
        translate([0, dx/2, 0])
            chain_unit();
}

module cutouts() {
    for(side = [-1, 1]) {
    translate([inf, side*distance/4, -top_margin + margin]) {
        rotate([0, -90, 0])
        rotate([0,0,-90]) {
            linear_extrude(height=inf, convexity=100) {
                polygon(points=[[0, 0], [-top_margin/2, top_margin/2], [-top_margin/2,top_margin-2*margin], [top_margin/2,top_margin-2*margin], [top_margin/2, top_margin/2]]);
            }
        }
    }
    }
}

difference() {
    translate([0, -width/2, -margin]) {
        cube([length, width, margin]);
    }
    translate([length - 2.5, 0, -margin*2]) {
        $fn=100;
        cylinder(r=3/2, h=margin*2);
    }
}

translate([0, 0, -top_margin-16]) {
    difference() {
        connectors();
        translate([-40, -distance/2, 0])
            cube([40, distance, 80]);
    }
}
difference() {
    translate([0,-(distance/2 + 20 + 4 + 0.5)/2, -top_margin])
        cube([18, distance/2 + 20 + 4 + 0.5, top_margin]);
    cutouts();
}